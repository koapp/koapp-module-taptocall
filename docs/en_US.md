# **(How to) Configure the Tap to Call module**

This module has 4 configurable fields:

| Field  | Value  | Description  |   
|---|---|---|---|---|
| Phone Number  | Phone Number  | Phone you'd like to make a phone call  |
| Button text  | string  |  Text to be displayed in the button |
| Button type  | Standard / Big  | Button type  |
| Button position  | Left / Center / Right  | Button position  |
