# **(How to) Configurar el módulo Tap to Call**

Este módulo tiene 4 campos configurables:

| Campo  | Valores  | Descripción  |   
|---|---|---|---|---|
| Phone Number  | Número de teléfono  | Número de teléfono al que hacer la llamada  |
| Button text  | string  |  Texto que quieres que se muestre en el botón |
| Button type  | Standard / Big  | Tipo de botón  |
| Button position  | Left / Center / Right  | Posición del botón  |
