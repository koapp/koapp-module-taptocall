(function() {
  'use strict';

  angular
    .module('taptocall', [])
    .controller('taptocallController', loadFunction);

  loadFunction.$inject = ['$scope', 'structureService', '$location'];

  function loadFunction($scope, structureService, $location) {
    structureService.registerModule($location, $scope, 'taptocall');
    $scope.phoneNumber = $scope.taptocall.modulescope.phoneNumber;
    $scope.buttonType = $scope.taptocall.modulescope.buttonType;
    $scope.caption = $scope.taptocall.modulescope.caption;
    $scope.position = $scope.taptocall.modulescope.position;
    $scope.goToLink = function(data){
     window.open(data, '_system');
    }
  }
}());
